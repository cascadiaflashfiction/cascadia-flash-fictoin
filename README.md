The Cascadia Flash Fiction Contest is a monthly writing competition that focuses on story, discovery, and author exposure. All genres welcome. Now accepting submissions!

Address : 19446 Hollygrape Street, Bend, OR 97702

Phone : 541-280-8764